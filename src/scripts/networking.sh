#!/bin/sh -eux

echo 'RES_OPTIONS="single-request-reopen"' >>/etc/sysconfig/network;

# determine the major EL version we're runninng
major_version="`sed 's/^.\+ release \([.0-9]\+\).*/\1/' /etc/redhat-release | awk -F. '{print $1}'`";

if [ "$major_version" -ge 8 ]; then
    nmcli networking off
    sleep 5
    nmcli networking on
else
    service network restart;
fi

echo 'Slow DNS fix applied (single-request-reopen)';
