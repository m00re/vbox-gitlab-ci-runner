# Vagrant Box: CentOS-based Gitlab CI-Runner

This repository contains [Packer.io](https://www.packer.io/) build templates for CentOS-based Gitlab
CI runner. It includes the runner itself and a working Docker subsystem, such that the runner can
be configured to use the ```docker``` executor.

## Build dependencies

- Packer.io version [1.7.0](https://www.packer.io/downloads.html) or higher
- Vagrant version [2.2.x](https://www.vagrantup.com/downloads.html) or higher

## Building the box

### CentOS 8 Stream
```
$ packer build centos-8.json
$ vagrant box add centos-8-stream-gitlab-ci-runner target/virtualbox-centos-8-stream-gitlab-ci-runner.box
``` 

## Published images in Vagrant cloud

Box Name                                 | Version | CentOS | Gitlab Runner | Docker
-----------------------------------------|---------|--------|---------------|-----------------------
m00re/centos-7-gitlab-ci-runner          | 0       | 1810   | 12.0.2-1      | 18.09.7, build 2d0083d
m00re/centos-8-stream-gitlab-ci-runner   | 0       | 1810   | 12.0.2-1      | 18.09.7, build 2d0083d

See also https://app.vagrantup.com/m00re/boxes/centos-7-gitlab-ci-runner/ and https://app.vagrantup.com/m00re/boxes/centos-8-stream-gitlab-ci-runner

## How to use this box

The box can be used as shown in the example below. The configuration registers the runner upon 
box provisioning, hence it is not required to log into the box and register the runner manually.

```ruby
require 'securerandom'

# Configure Gitlab Runner here
options = {}
options[:gitlab_runner_name] = SecureRandom.uuid
options[:gitlab_url] = 'https://gitlab.com/'
options[:gitlab_registration_token] = '<PUT_YOUR_REGISTRATION_TOKEN_HERE>'
options[:gitlab_tag_list] = 'docker'

gitlab_runner_command = "sudo gitlab-runner register " \
                        "--non-interactive " \
                        "--url \"%s\" " \
                        "--registration-token \"%s\" " \
                        "--executor \"docker\" " \
                        "--docker-image \"docker:stable\" " \
                        "--docker-privileged " \
                        "--tag-list \"%s\" " \
                        "--run-untagged=\"true\" " \
                        "--locked=\"false\" " \
                        "--access-level=\"not_protected\" " \
                        "--name=\"%s\"" % [options[:gitlab_url], options[:gitlab_registration_token], options[:gitlab_tag_list], options[:gitlab_runner_name]]

# Bootstrap Vagrant and configure the box
Vagrant.configure("2") do |config|

  config.vm.box = "m00re/centos-8-stream-gitlab-ci-runner"
  config.vm.box_version = "0"
  config.vm.synced_folder '.', '/vagrant', disabled: true

  config.vm.provider "virtualbox" do |vb|
    vb.gui = false
    vb.memory = "4096"
    vb.cpus = 2
    vb.customize ["modifyvm", :id, "--natsettings1", "1500,64,64,64,1024"]
    vb.customize ["modifyvm", :id, "--nictype1", "virtio"]
  end

  # Register the Gitlab-Runner during initial provisioning
  config.vm.provision "shell", inline: gitlab_runner_command

end
```

## Acknowledgements

The packer templates are based on the CentOS templates developed by the Boxcutter and/or Bento project. See 
https://github.com/boxcutter/centos and https://github.com/chef/bento
